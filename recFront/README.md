# Modelo para auxiliar na construção do Front - Rec 2023

## Clonando o Projeto

- Abra o terminal e execute o seguinte comando para clonar o repositório:

```bash
git clone https://gitlab.com/1a_ds_2semestre2023/exemplos/modelofront_rec_2023.git
```

## Iniciando o Projeto
- Instalando Dependências:
Entre no diretório do projeto e instale as dependências utilizando o comando npm:

```bash
cd modelofront_rec_2023/recfront
npm install
```

## Iniciando a página
- Após instalar as dependências, você pode iniciar a página com o seguinte comando:
```bash
npm run dev
```
<div align="center">BOA SORTE!!!</div>
